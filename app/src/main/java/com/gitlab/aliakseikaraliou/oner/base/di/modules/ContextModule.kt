package com.gitlab.aliakseikaraliou.oner.base.di.modules

import android.content.Context
import com.gitlab.aliakseikaraliou.oner.base.di.scopes.AppScope
import dagger.Module
import dagger.Provides

@Module
class ContextModule(val context: Context){

    @AppScope
    @Provides
    fun provideContext(): Context {
        return context
    }
}