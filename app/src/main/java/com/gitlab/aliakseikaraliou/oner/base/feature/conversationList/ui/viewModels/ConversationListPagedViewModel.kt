package com.gitlab.aliakseikaraliou.oner.base.feature.conversationList.ui.viewModels

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.paging.DataSource
import androidx.paging.PagedList
import com.gitlab.aliakseikaraliou.oner.base.models.Conversation
import com.gitlab.aliakseikaraliou.oner.base.models.ConversationModel
import com.gitlab.aliakseikaraliou.oner.base.source.Source
import com.gitlab.aliakseikaraliou.oner.base.utils.paging.createPageListLiveData

abstract class ConversationListPagedViewModel(private val dataSourceFactory: DataSource.Factory<*, Conversation>) : ViewModel(), BaseConversationListViewModel<ConversationModel> {
    override val liveData: MediatorLiveData<Source<ConversationModel>> = MediatorLiveData()

    protected abstract val config: PagedList.Config

    var onPagedListSubmit: (PagedList<Conversation>) -> Unit = {}

    private val pageLiveData by lazy {
        createPageListLiveData(dataSourceFactory, config)
    }

    private val observer = Observer<PagedList<Conversation>> { paged ->
        paged?.let { onPagedListSubmit(it) }
    }

    override fun loadConversations() {
        pageLiveData.observeForever(observer)
    }

    override fun onCleared() {
        pageLiveData.removeObserver(observer)

        super.onCleared()
    }
}