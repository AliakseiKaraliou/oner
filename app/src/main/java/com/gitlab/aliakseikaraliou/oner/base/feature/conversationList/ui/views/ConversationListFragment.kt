package com.gitlab.aliakseikaraliou.oner.base.feature.conversationList.ui.views

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.gitlab.aliakseikaraliou.oner.R
import com.gitlab.aliakseikaraliou.oner.base.OnerApplication
import com.gitlab.aliakseikaraliou.oner.base.feature.conversationList.di.components.ConversationListComponent
import com.gitlab.aliakseikaraliou.oner.base.feature.conversationList.di.components.DaggerConversationListComponent
import com.gitlab.aliakseikaraliou.oner.base.feature.conversationList.ui.viewModels.ConversationListPagedViewModel
import com.gitlab.aliakseikaraliou.oner.base.feature.conversationList.utils.adapters.adapter.ConversationListPagedAdapter
import com.gitlab.aliakseikaraliou.oner.base.models.ConversationModel
import com.gitlab.aliakseikaraliou.oner.base.source.Source
import com.gitlab.aliakseikaraliou.oner.base.ui.base.BaseFragment
import com.gitlab.aliakseikaraliou.oner.base.utils.extension.isVisible
import kotlinx.android.synthetic.main.fragment_dialog_list.*

abstract class ConversationListFragment : BaseFragment() {
    override val layoutId = R.layout.fragment_dialog_list

    protected abstract val viewModel: ConversationListPagedViewModel

    private val conversationListComponent: ConversationListComponent
        get() = DaggerConversationListComponent
                .builder()
                .appComponent(OnerApplication.get(baseActivity).appComponent)
                .build()

    private val recyclerViewAdapter = ConversationListPagedAdapter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setupViewModel(conversationListComponent)

        super.onViewCreated(view, savedInstanceState)

        initRecyclerView()

        viewModel.onPagedListSubmit = {
            recyclerViewAdapter.submitList(it)
        }

        viewModel.liveData.observe(this, Observer<Source<ConversationModel>> { presentationModel ->
            presentationModel?.also { presentation ->
                when {
                    presentation.loading() -> {
                        showProgress()
                    }
                    presentation.success() -> {
                        hideProgress()
                    }
                    presentation.error() -> {
                        hideProgress()
                    }
                }
            }
        })

    }

    private fun initRecyclerView() {
        recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            this.adapter = recyclerViewAdapter
        }
    }

    open fun setupViewModel(conversationListComponent: ConversationListComponent) {}

    fun loadConversations() {
        viewModel.loadConversations()
    }

    private fun showProgress() {
        progress.isVisible = true
    }

    private fun hideProgress() {
        progress.isVisible = false
    }
}