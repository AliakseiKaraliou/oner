package com.gitlab.aliakseikaraliou.oner.base.feature.conversationList.utils.adapters.adapter

import androidx.paging.PagedListAdapter
import android.view.LayoutInflater
import android.view.ViewGroup
import com.gitlab.aliakseikaraliou.oner.R
import com.gitlab.aliakseikaraliou.oner.base.feature.conversationList.utils.adapters.viewHolder.ConversationItemViewHolder
import com.gitlab.aliakseikaraliou.oner.base.feature.conversationList.utils.diffUtils.ConversationDiffUtilsItemsCallback
import com.gitlab.aliakseikaraliou.oner.base.models.Conversation

class ConversationListPagedAdapter : PagedListAdapter<Conversation, ConversationItemViewHolder>(ConversationDiffUtilsItemsCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ConversationItemViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_conversation, parent, false)
        return ConversationItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: ConversationItemViewHolder, position: Int) {
        getItem(position)?.let { holder.bind(it) }
    }
}