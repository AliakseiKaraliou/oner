package com.gitlab.aliakseikaraliou.oner.base.feature.conversationList.utils.adapters.viewHolder

import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.gitlab.aliakseikaraliou.oner.base.models.Conversation
import com.gitlab.aliakseikaraliou.oner.databinding.ItemConversationBinding

class ConversationItemViewHolder(itemView: View) : ViewHolder(itemView) {
    private val binding = DataBindingUtil.bind<ItemConversationBinding>(itemView)

     fun bind(item: Conversation) {
        binding?.conversation = item
    }
}