package com.gitlab.aliakseikaraliou.oner.base.models

interface Conversation {
    val receiver: Receiver
    val lastMessage: Message?
    val isPinned: Boolean
    val unreadCount: Int

    val title
        get() = receiver.fullName

    val id
        get() = receiver.id
}