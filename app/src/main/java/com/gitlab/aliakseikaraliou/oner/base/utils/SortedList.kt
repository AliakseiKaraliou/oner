package com.gitlab.aliakseikaraliou.oner.base.utils

import java.util.ArrayList

class SortedList<T>(private val sorting: (val1: T, val2: T) -> Boolean,
                    private val descending: Boolean = false) : ArrayList<T>() {

    override fun add(element: T): Boolean {
        for (i in 0 until this.size) {
            val item = this[i]

            if (sorting(element, item) || (!sorting(element, item) && descending)) {
                super.add(i, element)
                return true
            }
        }

        return super.add(element)
    }

    override fun add(index: Int, element: T) {
        throw UnsupportedOperationException()
    }
}
