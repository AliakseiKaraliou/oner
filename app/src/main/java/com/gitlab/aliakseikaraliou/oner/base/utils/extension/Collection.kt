package com.gitlab.aliakseikaraliou.oner.base.utils.extension

fun <T> Collection<T>.forEachUntil(until: (T) -> Boolean, action: (T) -> Unit) {
    for (element in this) {
        if (until(element)) {
            break
        }

        action(element)
    }
}