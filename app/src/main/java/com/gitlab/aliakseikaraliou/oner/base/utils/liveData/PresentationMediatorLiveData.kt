package com.gitlab.aliakseikaraliou.oner.base.utils.liveData

import androidx.lifecycle.MediatorLiveData
import com.gitlab.aliakseikaraliou.oner.base.source.Source

class PresentationMediatorLiveData<T> : MediatorLiveData<Source<T>>()