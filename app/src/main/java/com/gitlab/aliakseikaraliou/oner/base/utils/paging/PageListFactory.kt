@file:Suppress("unused")

package com.gitlab.aliakseikaraliou.oner.base.utils.paging

import androidx.paging.DataSource
import androidx.paging.PagedList
import java.util.concurrent.Executors
import androidx.paging.LivePagedListBuilder

fun <T, V> createPageList(dataSource: DataSource<T, V>, config: PagedList.Config) = PagedList.Builder(dataSource, config)
        .setNotifyExecutor(MainThreadExecutor())
        .setFetchExecutor(Executors.newSingleThreadExecutor())
        .build()

fun <T, V> createPageListLiveData(dataSourceFactory: DataSource.Factory<T, V>, config: PagedList.Config) = LivePagedListBuilder(dataSourceFactory, config)
        .setFetchExecutor(Executors.newSingleThreadExecutor())
        .build()