package com.gitlab.aliakseikaraliou.oner.sms.db

import android.content.Context
import com.gitlab.aliakseikaraliou.oner.sms.db.repositories.SmsContactRepository
import com.gitlab.aliakseikaraliou.oner.sms.db.repositories.SmsConversationModelRepository

class SmsRepositoryFactory(val context: Context) {
    val userRepository = SmsContactRepository(context)
    val conversationModelRepository = SmsConversationModelRepository(context,
            userRepository)
}