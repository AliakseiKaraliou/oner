package com.gitlab.aliakseikaraliou.oner.sms.db.repositories

import android.content.Context
import android.net.Uri
import android.provider.ContactsContract
import com.gitlab.aliakseikaraliou.oner.base.utils.extension.get
import com.gitlab.aliakseikaraliou.oner.sms.models.clean.authors.SmsContact

class SmsContactRepository(val context: Context) {
    fun loadContactByNumber(id: Long): SmsContact {
        val uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(id.toString()))
        
        return context.contentResolver.query(uri,
                null,
                null,
                null,
                null)
                ?.takeIf { it.moveToFirst() }
                ?.use { cursor ->
                    val contactId = cursor.get<Long>(ID)
                    val displayName = cursor.get<String>(DISPLAY_NAME)
                    val photoUrl = cursor.get<String?>(PHOTO_THUMB_URI)
                    
                    SmsContact(id = contactId,
                            fullName = displayName,
                            phoneNumber = id.toString(),
                            imageUrl = photoUrl)
                }
                ?: run {
                    SmsContact(id = -1,
                            fullName = id.toString(),
                            phoneNumber = id.toString(),
                            imageUrl = null)
                }
    }
    
    private companion object {
        const val ID = "_id"
        const val DISPLAY_NAME = "display_name"
        const val PHOTO_THUMB_URI = "photo_thumb_uri"
    }
}