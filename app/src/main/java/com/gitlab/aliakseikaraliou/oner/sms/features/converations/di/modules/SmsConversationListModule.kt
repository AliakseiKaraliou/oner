package com.gitlab.aliakseikaraliou.oner.sms.features.converations.di.modules

import androidx.lifecycle.ViewModelProviders
import com.gitlab.aliakseikaraliou.oner.base.db.BaseRepositoryFactory
import com.gitlab.aliakseikaraliou.oner.base.feature.conversationList.ui.viewModels.ConversationListPagedViewModel
import com.gitlab.aliakseikaraliou.oner.sms.db.repositories.SmsConversationModelRepository
import com.gitlab.aliakseikaraliou.oner.sms.features.converations.dataSource.SmsConversationModelDataSourceFactory
import com.gitlab.aliakseikaraliou.oner.sms.features.converations.di.scopes.SmsListScope
import com.gitlab.aliakseikaraliou.oner.sms.features.converations.ui.view.SmsConversationListFragment
import com.gitlab.aliakseikaraliou.oner.sms.features.converations.ui.viewModel.SmsConversationListViewModel
import com.gitlab.aliakseikaraliou.oner.sms.features.converations.ui.viewModel.SmsConversationViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class SmsConversationListModule {

    @SmsListScope
    @Provides
    fun provideRepository(database: BaseRepositoryFactory) = database.sms.conversationModelRepository

    @SmsListScope
    @Provides
    fun provideDataSourceFactory(repository: SmsConversationModelRepository) = SmsConversationModelDataSourceFactory(repository)

    @SmsListScope
    @Provides
    fun provideViewModel(fragment: SmsConversationListFragment,
                         dataSourceFactory: SmsConversationModelDataSourceFactory)
            : ConversationListPagedViewModel {
        val viewModelFactory = SmsConversationViewModelFactory(dataSourceFactory)

        return ViewModelProviders.of(fragment, viewModelFactory)
                .get(SmsConversationListViewModel::class.java)
    }
}