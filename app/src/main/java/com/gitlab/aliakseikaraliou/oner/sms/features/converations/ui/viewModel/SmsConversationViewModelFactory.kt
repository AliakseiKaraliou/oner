package com.gitlab.aliakseikaraliou.oner.sms.features.converations.ui.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.gitlab.aliakseikaraliou.oner.sms.features.converations.dataSource.SmsConversationModelDataSourceFactory

@Suppress("UNCHECKED_CAST")
class SmsConversationViewModelFactory(private val dataSourceFactory: SmsConversationModelDataSourceFactory) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>) = when (modelClass) {
        SmsConversationListViewModel::class.java -> SmsConversationListViewModel(dataSourceFactory)
        else -> throw IllegalArgumentException()
    } as T
    
}