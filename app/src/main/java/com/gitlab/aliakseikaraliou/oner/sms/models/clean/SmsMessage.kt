package com.gitlab.aliakseikaraliou.oner.sms.models.clean

import com.gitlab.aliakseikaraliou.oner.base.models.Chat
import com.gitlab.aliakseikaraliou.oner.base.models.Message
import com.gitlab.aliakseikaraliou.oner.sms.models.clean.authors.SmsAuthor
import java.util.Date

data class SmsMessage(override val id: Long,
                      override val text: String?,
                      override val author: SmsAuthor,
                      override val date: Date,
                      override val isRead: Boolean,
                      override val isOut: Boolean) : Message {
    override val chat: Chat? = null
}