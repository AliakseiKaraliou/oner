package com.gitlab.aliakseikaraliou.oner.sms.models.clean.authors

import com.gitlab.aliakseikaraliou.oner.base.models.Author

interface SmsAuthor : Author
