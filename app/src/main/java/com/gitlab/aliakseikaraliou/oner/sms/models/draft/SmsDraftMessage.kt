package com.gitlab.aliakseikaraliou.oner.sms.models.draft

import com.gitlab.aliakseikaraliou.oner.sms.models.clean.SmsMessage
import com.gitlab.aliakseikaraliou.oner.sms.models.clean.authors.SmsAuthor
import java.util.Date

data class SmsDraftMessage(val id: Long,
                           val threadId: Int,
                           val text: String?,
                           val address: String,
                           val date: Date,
                           val isRead: Boolean,
                           val isOut: Boolean) {

    fun create(author: SmsAuthor) = SmsMessage(id = id,
            text = text,
            author = author,
            date = date,
            isRead = isRead,
            isOut = isOut)
}