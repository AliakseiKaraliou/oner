package com.gitlab.aliakseikaraliou.oner.telegram.auth.ui.fragments

import android.os.Bundle
import android.view.View
import com.gitlab.aliakseikaraliou.oner.BuildConfig
import com.gitlab.aliakseikaraliou.oner.R
import com.gitlab.aliakseikaraliou.oner.base.utils.PhoneNumberUtils
import com.gitlab.aliakseikaraliou.oner.base.utils.extension.isVisible
import kotlinx.android.synthetic.main.fragment_telegram_auth_phone.*

class TelegramAuthorizationPhoneFragment : BaseTelegramAuthorizationFragment() {
    override val layoutId: Int
        get() = R.layout.fragment_telegram_auth_phone

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        button.setOnClickListener {
            val phoneNumber = phoneNumberView.text.toString()

            if (PhoneNumberUtils.isValid(phoneNumber)) {
                viewModel.authorizator.phoneNumber = phoneNumber
            } else {
                phoneNumberView.error = getString(R.string.telegram_error_auth_phonenumberinvalid)
            }
        }

        if (BuildConfig.DEBUG){
            phoneHint.isVisible=true

            phoneHint.setOnClickListener {
                phoneNumberView.setText("+375336837494")
            }
        }
    }
}