package com.gitlab.aliakseikaraliou.oner.telegram.auth.viewmodel.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.gitlab.aliakseikaraliou.oner.telegram.auth.models.TelegramAuthorizator
import com.gitlab.aliakseikaraliou.oner.telegram.auth.viewmodel.TelegramAuthorizationViewModel

class TelegramAuthorizationViewModelFactory(private val authorizator: TelegramAuthorizator)
    : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return when (modelClass) {
            TelegramAuthorizationViewModel::class.java ->
                TelegramAuthorizationViewModel(authorizator) as T
            else -> super.create(modelClass)
        }
    }
}