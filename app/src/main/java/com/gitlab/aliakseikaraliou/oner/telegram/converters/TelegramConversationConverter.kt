package com.gitlab.aliakseikaraliou.oner.telegram.converters

import com.gitlab.aliakseikaraliou.oner.telegram.models.draft.conversations.TelegramChannelDraftConversation
import com.gitlab.aliakseikaraliou.oner.telegram.models.draft.conversations.TelegramChatDraftConversation
import com.gitlab.aliakseikaraliou.oner.telegram.models.draft.conversations.TelegramDraftConversation
import com.gitlab.aliakseikaraliou.oner.telegram.models.draft.conversations.TelegramPrivateDraftConversation
import org.drinkless.td.libcore.telegram.TdApi

object TelegramConversationConverter {
    fun convert(chat: TdApi.Chat): TelegramDraftConversation {
        return when (chat.type) {
            is TdApi.ChatTypePrivate -> parsePrivate(chat, false)
            is TdApi.ChatTypeSupergroup -> parseSuperGroup(chat, chat.type as TdApi.ChatTypeSupergroup)
            is TdApi.ChatTypeBasicGroup -> parseChat(chat)
            is TdApi.ChatTypeSecret -> parsePrivate(chat, true)
            else -> throw IllegalArgumentException(chat.type.javaClass.name)
        }
    }

    private fun parseSuperGroup(chat: TdApi.Chat, type: TdApi.ChatTypeSupergroup): TelegramDraftConversation {
        return if (type.isChannel) {
            parseChannel(chat)
        } else {
            parseChat(chat)
        }
    }

    private fun parsePrivate(chat: TdApi.Chat, isSecret: Boolean): TelegramPrivateDraftConversation {
        val lastMessage = TelegramMessageConverter
                .convert(chat.lastMessage, chat.type)

        return TelegramPrivateDraftConversation(authorId = lastMessage?.authorId ?: chat.id,
                draftMessage = lastMessage,
                isSecret = isSecret,
                isPinned = chat.isPinned,
                unreadCount = chat.unreadCount,
                order = chat.order)
    }

    private fun parseChannel(chat: TdApi.Chat): TelegramChannelDraftConversation {
        val lastMessage = TelegramMessageConverter
                .convert(chat.lastMessage, chat.type)

        return TelegramChannelDraftConversation(authorId = chat.id,
                draftLastMessage = lastMessage,
                isPinned = chat.isPinned,
                unreadCount = chat.unreadCount,
                order = chat.order)
    }

    private fun parseChat(chat: TdApi.Chat): TelegramChatDraftConversation {
        val lastMessage = TelegramMessageConverter
                .convert(chat.lastMessage, chat.type)

        return TelegramChatDraftConversation(chatId = chat.id,
                userId = lastMessage?.authorId,
                lastMessage = lastMessage,
                isPinned = chat.isPinned,
                unreadCount = chat.unreadCount,
                order = chat.order)
    }

}
