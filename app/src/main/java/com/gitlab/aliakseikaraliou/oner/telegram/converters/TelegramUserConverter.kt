package com.gitlab.aliakseikaraliou.oner.telegram.converters

import com.gitlab.aliakseikaraliou.oner.base.utils.dateFromTimestamp
import com.gitlab.aliakseikaraliou.oner.telegram.models.clean.TelegramOnlineStatus
import com.gitlab.aliakseikaraliou.oner.telegram.models.draft.authors.TelegramDraftUser
import org.drinkless.td.libcore.telegram.TdApi

object TelegramUserConverter {
    fun convert(user: TdApi.User): TelegramDraftUser {
        val photoId = user.profilePhoto?.small?.id?.takeIf { it > 0 }
        val localPath = user.profilePhoto?.small?.local?.path?.takeIf { it.isNotEmpty() }
        val isBot = user.type is TdApi.UserTypeBot

        val onlineStatus = if (isBot) TelegramOnlineStatus.offline() else parseOnline(user.status)

        return TelegramDraftUser(id = user.id.toLong(),
                firstName = user.firstName,
                lastName = user.lastName,
                username = user.username,
                imageUrl = localPath,
                imageId = photoId,
                onlineStatus = onlineStatus,
                isBot = isBot)
    }

    private fun parseOnline(status: TdApi.UserStatus?) = when (status) {
        is TdApi.UserStatusOnline -> TelegramOnlineStatus.online()
        is TdApi.UserStatusOffline -> TelegramOnlineStatus.offline(dateFromTimestamp(status.wasOnline.toLong()))
        else -> TelegramOnlineStatus.offline()
    }
}