package com.gitlab.aliakseikaraliou.oner.telegram.db

import com.gitlab.aliakseikaraliou.oner.telegram.db.repositories.*

class TelegramRepositoryFactory {
    private val downloadFileRepository = TelegramDownloadFileRepository()

    private val chatRepository = TelegramChatRepository(downloadFileRepository)
    private val userRepository = TelegramUserRepository(downloadFileRepository)
    private val channelRepository = TelegramChannelRepository(downloadFileRepository)

    val conversationModelRepository = TelegramConversationModelRepository(userRepository,
            channelRepository,
            chatRepository)
}