package com.gitlab.aliakseikaraliou.oner.telegram.db.repositories

import androidx.lifecycle.LiveData
import com.gitlab.aliakseikaraliou.oner.telegram.execution.executables.TelegramChatExecutable
import com.gitlab.aliakseikaraliou.oner.telegram.models.clean.authors.TelegramChat
import kotlinx.coroutines.experimental.async

class TelegramChatRepository(private val downloadFileRepository: TelegramDownloadFileRepository) {
    suspend fun loadChat(id: Long): TelegramChat {
        val userExecutable = TelegramChatExecutable(id)
        val draftChat = userExecutable.execute()

        return when {
            draftChat.imageUrl != null -> {
                draftChat.build()
            }
            draftChat.imageId != null -> {
                val async = async {
                    downloadFileRepository.downloadFile(draftChat.imageId)
                }

                draftChat.build(async.await().localPath)
            }
            else -> {
                draftChat.build()
            }
        }
    }
}