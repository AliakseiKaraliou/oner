package com.gitlab.aliakseikaraliou.oner.telegram.db.repositories

import com.gitlab.aliakseikaraliou.oner.telegram.execution.executables.TelegramDownloadFileExecutable
import com.gitlab.aliakseikaraliou.oner.telegram.models.TelegramPriority
import com.gitlab.aliakseikaraliou.oner.telegram.models.clean.TelegramFile

class TelegramDownloadFileRepository{
    suspend fun downloadFile(fileId: Int): TelegramFile {
        val downloadFileExecutable = TelegramDownloadFileExecutable(
                fileId,
                TelegramPriority.HIGH)

        return downloadFileExecutable.execute()
    }

}