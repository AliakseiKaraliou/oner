package com.gitlab.aliakseikaraliou.oner.telegram.db.repositories

import com.gitlab.aliakseikaraliou.oner.telegram.execution.executables.TelegramUserExecutable
import com.gitlab.aliakseikaraliou.oner.telegram.models.clean.authors.TelegramUser
import kotlinx.coroutines.experimental.async

class TelegramUserRepository(private val downloadFileRepository: TelegramDownloadFileRepository) {
    suspend fun loadUser(id: Long): TelegramUser {
        val userExecutable = TelegramUserExecutable(id)
        val draftUser = userExecutable.execute()

        return when {
            draftUser.imageUrl != null -> {
                draftUser.build()
            }
            draftUser.imageId != null -> {
                val async = async {
                    downloadFileRepository.downloadFile(draftUser.imageId)
                }

                draftUser.build(async.await().localPath)
            }
            else -> {
                draftUser.build()
            }
        }
    }


}