package com.gitlab.aliakseikaraliou.oner.telegram.execution.exception

import org.drinkless.td.libcore.telegram.TdApi

open class TelegramException : Throwable {
    constructor(message: String) : super(message)
    constructor(throwable: Throwable) : super(throwable)
}

class TelegramApiException(override val message: String, val code: Int) : TelegramException(message) {
    constructor(error: TdApi.Error) : this(code = error.code,
            message = error.message)
}