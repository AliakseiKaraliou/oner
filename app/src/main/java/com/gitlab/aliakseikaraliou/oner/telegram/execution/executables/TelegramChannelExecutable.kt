package com.gitlab.aliakseikaraliou.oner.telegram.execution.executables

import com.gitlab.aliakseikaraliou.oner.telegram.converters.TelegramChannelConverter
import com.gitlab.aliakseikaraliou.oner.telegram.models.draft.authors.TelegramDraftChannel
import org.drinkless.td.libcore.telegram.TdApi

class TelegramChannelExecutable(val chatId: Long)
    : BaseTelegramExecutable<TdApi.Chat, TelegramDraftChannel>() {
    override val function = TdApi.GetChat(chatId)
    
    override fun convertData(it: TdApi.Chat) = TelegramChannelConverter.convert(it)
    
}