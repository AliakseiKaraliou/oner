package com.gitlab.aliakseikaraliou.oner.telegram.features.conversations.list.di.components

import com.gitlab.aliakseikaraliou.oner.telegram.features.conversations.list.di.modules.TelegramConversationListModule
import com.gitlab.aliakseikaraliou.oner.telegram.features.conversations.list.di.scopes.TelegramListScope
import com.gitlab.aliakseikaraliou.oner.telegram.features.conversations.list.ui.view.TelegramConversationListFragment
import dagger.BindsInstance
import dagger.Subcomponent

@TelegramListScope
@Subcomponent(modules = [TelegramConversationListModule::class])
interface TelegramListComponent {
    fun inject(fragment: TelegramConversationListFragment)
    
    @Subcomponent.Builder
    interface Builder {
        @BindsInstance
        fun fragment(fragment: TelegramConversationListFragment): Builder
        
        fun build(): TelegramListComponent
    }
}
