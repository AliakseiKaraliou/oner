package com.gitlab.aliakseikaraliou.oner.telegram.models

enum class TelegramPriority(val value: Int) {
    HIGH(32)
}