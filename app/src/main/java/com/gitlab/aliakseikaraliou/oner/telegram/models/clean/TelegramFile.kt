package com.gitlab.aliakseikaraliou.oner.telegram.models.clean

data class TelegramFile(val id: Int,
                        val localPath: String)