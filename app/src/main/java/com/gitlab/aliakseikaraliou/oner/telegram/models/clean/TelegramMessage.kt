package com.gitlab.aliakseikaraliou.oner.telegram.models.clean

import com.gitlab.aliakseikaraliou.oner.base.models.Message
import com.gitlab.aliakseikaraliou.oner.telegram.models.clean.authors.TelegramAuthor
import com.gitlab.aliakseikaraliou.oner.telegram.models.clean.authors.TelegramChat
import java.util.Date

data class TelegramMessage(override val id: Long,
                           override val text: String?,
                           override var author: TelegramAuthor?,
                           override val chat: TelegramChat? = null,
                           override val date: Date,
                           override val isRead: Boolean,
                           val signature: String,
                           override val isOut: Boolean) : Message