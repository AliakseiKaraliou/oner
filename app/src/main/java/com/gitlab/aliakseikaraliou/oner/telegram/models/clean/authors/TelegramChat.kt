package com.gitlab.aliakseikaraliou.oner.telegram.models.clean.authors

import com.gitlab.aliakseikaraliou.oner.base.models.Chat

data class TelegramChat(override val id: Long,
                        override val fullName: String,
                        override val imageUrl: String?) : Chat, TelegramReceiver