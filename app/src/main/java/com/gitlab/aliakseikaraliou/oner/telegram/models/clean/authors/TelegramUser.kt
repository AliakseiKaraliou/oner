package com.gitlab.aliakseikaraliou.oner.telegram.models.clean.authors

import com.gitlab.aliakseikaraliou.oner.base.models.User
import com.gitlab.aliakseikaraliou.oner.telegram.models.clean.TelegramOnlineStatus

data class TelegramUser(override val id: Long,
                        override val firstName: String,
                        override val lastName: String,
                        override val username: String?,
                        override val imageUrl: String?,
                        val onlineStatus: TelegramOnlineStatus,
                        val isBot: Boolean) : User, TelegramAuthor {
    override val isOnlineMobile = false
    override val isOnlineWeb = onlineStatus.isOnline()
}