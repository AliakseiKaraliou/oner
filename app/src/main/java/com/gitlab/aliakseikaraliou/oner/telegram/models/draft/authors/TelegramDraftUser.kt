package com.gitlab.aliakseikaraliou.oner.telegram.models.draft.authors

import com.gitlab.aliakseikaraliou.oner.telegram.models.clean.TelegramOnlineStatus
import com.gitlab.aliakseikaraliou.oner.telegram.models.clean.authors.TelegramUser

data class TelegramDraftUser(val id: Long,
                             val firstName: String,
                             val lastName: String,
                             val username: String?,
                             val imageUrl: String?,
                             val imageId: Int?,
                             val onlineStatus: TelegramOnlineStatus,
                             val isBot: Boolean) {

    fun build(imageUrl: String? = null): TelegramUser {
        val imagePath = imageUrl ?: this.imageUrl
        val fullImagePath = imagePath?.let { "file://$it" }

        return TelegramUser(id = id,
                firstName = firstName,
                lastName = lastName,
                username = username,
                imageUrl = fullImagePath,
                onlineStatus = onlineStatus,
                isBot = isBot)
    }
}