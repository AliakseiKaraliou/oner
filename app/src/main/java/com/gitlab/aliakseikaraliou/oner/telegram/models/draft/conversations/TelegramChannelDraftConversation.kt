package com.gitlab.aliakseikaraliou.oner.telegram.models.draft.conversations

import com.gitlab.aliakseikaraliou.oner.telegram.models.clean.authors.TelegramChannel
import com.gitlab.aliakseikaraliou.oner.telegram.models.clean.conversations.TelegramChannelConversation
import com.gitlab.aliakseikaraliou.oner.telegram.models.draft.TelegramDraftMessage

data class TelegramChannelDraftConversation(val authorId: Long,
                                            val draftLastMessage: TelegramDraftMessage?,
                                            val isPinned: Boolean,
                                            val unreadCount: Int,
                                            val order: Long) : TelegramDraftConversation {

    fun convert(author: TelegramChannel): TelegramChannelConversation {
        if (author.id != authorId) {
            throw IllegalArgumentException()
        }

        val message = draftLastMessage?.convert(author, null)

        return TelegramChannelConversation(receiver = author,
                lastMessage = message,
                isPinned = isPinned,
                unreadCount = unreadCount,
                order = order)
    }

}