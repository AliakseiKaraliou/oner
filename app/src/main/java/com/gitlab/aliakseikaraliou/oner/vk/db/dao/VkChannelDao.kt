package com.gitlab.aliakseikaraliou.oner.vk.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import com.gitlab.aliakseikaraliou.oner.vk.db.entities.VkChannelEntity

@Suppress("unused")
@Dao
interface VkChannelDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(user: VkChannelEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(users: List<VkChannelEntity>)
}