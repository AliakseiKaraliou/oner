package com.gitlab.aliakseikaraliou.oner.vk.db.repositories

import com.gitlab.aliakseikaraliou.oner.vk.db.dao.VkChatDao
import com.gitlab.aliakseikaraliou.oner.vk.db.entities.VkChatEntity
import com.gitlab.aliakseikaraliou.oner.vk.db.repositories.models.VkPeerCache
import com.gitlab.aliakseikaraliou.oner.vk.models.clean.authors.VkChat
import com.gitlab.aliakseikaraliou.oner.vk.network.VkApi

class VkChatRepository(private val chatDao: VkChatDao,
                       private val api: VkApi,
                       private val peerCache: VkPeerCache) {
    suspend fun loadByChatIds(ids: List<Long>): MutableList<VkChat> {
        val chats = mutableListOf<VkChat>()

        if (ids.isEmpty()) {
            return chats
        }

        val freeIds = mutableListOf<Long>()

        ids.forEach { id ->
            (peerCache[id] as? VkChat)?.let {
                chats.add(it)
            } ?: run {
                freeIds.add(id)
            }
        }

        if (freeIds.isEmpty()) {
            return chats
        }

        val loaded = api.getChats(ids.joinToString(separator = ","))
                .execute()
                .takeIf { it.isSuccessful }
                ?.body()
                ?.list
                ?.also {loadedChats->
                    peerCache.putAll(loadedChats)
                    chatDao.insert(loadedChats.map { VkChatEntity(it) })
                } ?: listOf()

        chats.addAll(loaded)

        return chats
    }
}