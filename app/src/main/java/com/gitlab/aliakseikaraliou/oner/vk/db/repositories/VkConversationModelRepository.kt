package com.gitlab.aliakseikaraliou.oner.vk.db.repositories

import com.gitlab.aliakseikaraliou.oner.base.source.Source
import com.gitlab.aliakseikaraliou.oner.vk.models.clean.VkConversation
import com.gitlab.aliakseikaraliou.oner.vk.models.clean.VkMessage
import com.gitlab.aliakseikaraliou.oner.vk.models.clean.authors.VkAuthor
import com.gitlab.aliakseikaraliou.oner.vk.models.clean.authors.VkChat
import com.gitlab.aliakseikaraliou.oner.vk.models.clean.authors.VkConversationModel
import com.gitlab.aliakseikaraliou.oner.vk.models.clean.authors.VkReceiver
import com.gitlab.aliakseikaraliou.oner.vk.models.draft.VkDraftConversation
import com.gitlab.aliakseikaraliou.oner.vk.models.draft.VkDraftMessage
import com.gitlab.aliakseikaraliou.oner.vk.network.VkApi

class VkConversationModelRepository(private val api: VkApi,
                                    private val peerRepository: VkPeerRepository) {

    suspend fun loadConversations(offset: Int, count: Int): Source<VkConversationModel> {
        return api.getConversations(offset, count)
                .execute()
                .takeIf { it.isSuccessful }
                ?.body()
                ?.let { draftConversationModel ->
                    val draftConversations = draftConversationModel.conversations
                    val peerIds = parsePeerIds(draftConversations)

                    val receiverMap = peerRepository.loadByPeerId(peerIds)
                            .map { it.peerId to it }
                            .toMap()

                    val conversations = draftConversations.map { draftConversation ->
                        val peer = receiverMap[draftConversation.peerId]
                                ?: throw IllegalArgumentException("Provided id is not loaded")
                        val message = draftConversation.lastMessage?.let {
                            parseMessage(it, receiverMap)
                        }
                        val unreadCount = draftConversation.unreadCount

                        VkConversation(peer, message, unreadCount)
                    }

                    val conversationModel = VkConversationModel(
                            itemCount = draftConversationModel.count,
                            conversations = conversations)
                    Source.success(conversationModel)
                } ?: kotlin.run {
            Source.error<VkConversationModel>(null)
        }
    }

    private fun parseMessage(message: VkDraftMessage, receiverMap: Map<Long, VkReceiver>): VkMessage? {
        return (receiverMap[message.authorId] as? VkAuthor)?.let { author ->
            message.create(author = author,
                    chat = receiverMap[message.chatId] as? VkChat)
        }
    }

    private fun parsePeerIds(conversations: List<VkDraftConversation>): Collection<Long> {
        val peerIds = mutableSetOf<Long>()

        val userIds = conversations
                .map { it.peerId }
        peerIds.addAll(userIds)

        val chatUserIds = conversations
                .mapNotNull { it.lastMessage?.authorId }
        peerIds.addAll(chatUserIds)

        return peerIds
    }

}