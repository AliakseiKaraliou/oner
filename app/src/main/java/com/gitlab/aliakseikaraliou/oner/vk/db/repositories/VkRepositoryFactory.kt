package com.gitlab.aliakseikaraliou.oner.vk.db.repositories

import com.gitlab.aliakseikaraliou.oner.vk.BaseVkModel

@Suppress("MemberVisibilityCanBePrivate")
class VkRepositoryFactory(vkModel: BaseVkModel) {
    val userRepository = VkUserRepository(vkModel.database.userDao, vkModel.api, vkModel.peerCache)
    val chatRepository = VkChatRepository(vkModel.database.chatDao, vkModel.api, vkModel.peerCache)
    val channelRepository = VkChannelRepository(vkModel.database.channelDao, vkModel.api, vkModel.peerCache)
    val peerRepository = VkPeerRepository(userRepository, chatRepository, channelRepository)
    val conversationModelRepository = VkConversationModelRepository(vkModel.api, peerRepository)
}