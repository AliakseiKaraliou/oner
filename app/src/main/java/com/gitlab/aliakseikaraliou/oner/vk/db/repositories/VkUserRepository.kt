package com.gitlab.aliakseikaraliou.oner.vk.db.repositories

import com.gitlab.aliakseikaraliou.oner.vk.VkConstants
import com.gitlab.aliakseikaraliou.oner.vk.db.dao.VkUserDao
import com.gitlab.aliakseikaraliou.oner.vk.db.entities.VkUserEntity
import com.gitlab.aliakseikaraliou.oner.vk.db.repositories.models.VkPeerCache
import com.gitlab.aliakseikaraliou.oner.vk.models.clean.authors.VkUser
import com.gitlab.aliakseikaraliou.oner.vk.network.VkApi

class VkUserRepository(private val userDao: VkUserDao,
                       private val api: VkApi,
                       private val peerCache: VkPeerCache) {
    private val fields = arrayOf(VkConstants.Queries.SCREEN_NAME, VkConstants.Queries.PHOTO_100, VkConstants.Queries.ONLINE, VkConstants.Queries.LAST_SEEN)
            .joinToString(separator = ",")

    suspend fun loadUsersById(ids: Collection<Long>): List<VkUser> {
        val users = mutableListOf<VkUser>()

        if (ids.isEmpty()) {
            return users
        }

        val freeIds = mutableListOf<Long>()

        ids.forEach { id ->
            (peerCache[id] as? VkUser)?.let {
                users.add(it)
            } ?: run {
                freeIds.add(id)
            }
        }

        if (freeIds.isEmpty()) {
            return users
        }

        val loaded = api.getUsers(ids.joinToString(separator = ","), fields)
                .execute()
                .takeIf { it.isSuccessful }
                ?.body()
                ?.list
                ?.also { loadedUsers ->
                    peerCache.putAll(loadedUsers)
                    userDao.insert(loadedUsers.map { VkUserEntity(it) })
                } ?: listOf()
        users.addAll(loaded)

        return users
    }

}