package com.gitlab.aliakseikaraliou.oner.vk.di.modules

import androidx.room.Room
import android.content.Context
import com.gitlab.aliakseikaraliou.oner.base.di.scopes.AppScope
import com.gitlab.aliakseikaraliou.oner.vk.VkConfiguration
import com.gitlab.aliakseikaraliou.oner.vk.db.VkDatabase
import com.gitlab.aliakseikaraliou.oner.vk.db.repositories.models.VkChannelList
import com.gitlab.aliakseikaraliou.oner.vk.db.repositories.models.VkChatList
import com.gitlab.aliakseikaraliou.oner.vk.db.repositories.models.VkPeerCache
import com.gitlab.aliakseikaraliou.oner.vk.db.repositories.models.VkUserList
import com.gitlab.aliakseikaraliou.oner.vk.models.draft.VkDraftConversationModel
import com.gitlab.aliakseikaraliou.oner.vk.network.VkApi
import com.gitlab.aliakseikaraliou.oner.vk.network.deserializers.VkChannelDeserializer
import com.gitlab.aliakseikaraliou.oner.vk.network.deserializers.VkChatListDeserializer
import com.gitlab.aliakseikaraliou.oner.vk.network.deserializers.VkDraftConversationModelDeserializer
import com.gitlab.aliakseikaraliou.oner.vk.network.deserializers.VkUserListDeserializer
import com.gitlab.aliakseikaraliou.oner.vk.network.interceptors.VkQueryInterceptor
import com.gitlab.aliakseikaraliou.oner.vk.BaseVkModel
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
class VkModule {

    @AppScope
    @Provides
    fun providesVkQueryInterceptor() = VkQueryInterceptor()

    @AppScope
    @Provides
    fun providesHttpClient(interceptor: VkQueryInterceptor): OkHttpClient =
            OkHttpClient.Builder()
                    .addInterceptor(interceptor)
                    .build()

    @AppScope
    @Provides
    fun providesGson(): Gson {
        return GsonBuilder()
                .registerTypeAdapter(VkDraftConversationModel::class.java,
                        VkDraftConversationModelDeserializer())
                .registerTypeAdapter(VkUserList::class.java, VkUserListDeserializer())
                .registerTypeAdapter(VkChatList::class.java, VkChatListDeserializer())
                .registerTypeAdapter(VkChannelList::class.java, VkChannelDeserializer())
                .create()
    }

    @AppScope
    @Provides
    fun providesApi(okHttpClient: OkHttpClient, gson: Gson): VkApi = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .baseUrl(VkConfiguration.API_HOST)
            .client(okHttpClient)
            .build()
            .create(VkApi::class.java)

    @AppScope
    @Provides
    fun providePeerCache() = VkPeerCache()

    @AppScope
    @Provides
    fun provideVkDatabase(context: Context) = Room.databaseBuilder(context, VkDatabase::class.java, VkConfiguration.DB_NAME).build()

    @AppScope
    @Provides
    fun providesConfigurationModel(api: VkApi, peerCache: VkPeerCache, database: VkDatabase) = BaseVkModel(api, database, peerCache)

}