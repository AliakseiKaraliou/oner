package com.gitlab.aliakseikaraliou.oner.vk.features.conversations.dataSource

import androidx.lifecycle.MutableLiveData
import androidx.paging.PositionalDataSource
import com.gitlab.aliakseikaraliou.oner.base.models.Conversation
import com.gitlab.aliakseikaraliou.oner.base.source.Source
import com.gitlab.aliakseikaraliou.oner.vk.db.repositories.VkConversationModelRepository
import com.gitlab.aliakseikaraliou.oner.vk.models.clean.authors.VkConversationModel
import kotlinx.coroutines.experimental.launch

class VkConversationModelDataSource(val repository: VkConversationModelRepository) : PositionalDataSource<Conversation>() {

    val liveData = MutableLiveData<Source<VkConversationModel>>()

    override fun loadInitial(params: LoadInitialParams, callback: LoadInitialCallback<Conversation>) {
        liveData.postValue(Source.loading())

        launch {
            val conversationModel = repository.loadConversations(offset = params.requestedStartPosition, count = params.requestedLoadSize)

            liveData.postValue(conversationModel)
            conversationModel.item?.let {
                callback.onResult(it.conversations, params.requestedStartPosition)
            }
        }
    }

    override fun loadRange(params: PositionalDataSource.LoadRangeParams, callback: PositionalDataSource.LoadRangeCallback<Conversation>) {
        launch {
            val conversationModel = repository.loadConversations(offset = params.startPosition,
                    count = params.loadSize)

            liveData.postValue(conversationModel)
            conversationModel.item?.conversations?.let {
                callback.onResult(it)
            }
        }
    }
}