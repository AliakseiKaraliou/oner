package com.gitlab.aliakseikaraliou.oner.vk.features.conversations.di.components

import com.gitlab.aliakseikaraliou.oner.vk.features.conversations.di.modules.VkListModule
import com.gitlab.aliakseikaraliou.oner.vk.features.conversations.di.scopes.VkListScope
import com.gitlab.aliakseikaraliou.oner.vk.features.conversations.ui.view.VkConversationListFragment
import dagger.BindsInstance
import dagger.Subcomponent

@VkListScope
@Subcomponent(modules = [VkListModule::class])
interface VkListComponent {
    fun inject(fragment: VkConversationListFragment)
    
    @Subcomponent.Builder
    interface Builder {
        @BindsInstance
        fun fragment(fragment: VkConversationListFragment): Builder
        
        fun build(): VkListComponent
    }
}