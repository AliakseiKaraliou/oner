package com.gitlab.aliakseikaraliou.oner.vk.models.clean

import com.gitlab.aliakseikaraliou.oner.base.models.Conversation
import com.gitlab.aliakseikaraliou.oner.base.models.Message
import com.gitlab.aliakseikaraliou.oner.vk.models.clean.authors.VkReceiver

data class VkConversation(override val receiver: VkReceiver,
                          override val lastMessage: Message?,
                          override val unreadCount: Int) : Conversation {
    override val isPinned = false
}