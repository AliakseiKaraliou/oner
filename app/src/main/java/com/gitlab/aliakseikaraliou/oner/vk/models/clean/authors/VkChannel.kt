package com.gitlab.aliakseikaraliou.oner.vk.models.clean.authors

import com.gitlab.aliakseikaraliou.oner.base.models.Channel
import com.gitlab.aliakseikaraliou.oner.vk.utils.VkPeerer

data class VkChannel(override val id: Long,
                     override val fullName: String,
                     override val imageUrl: String?) : Channel, VkAuthor {
    override val peerId = VkPeerer.channelIdToPeerId(id)
}