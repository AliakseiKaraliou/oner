package com.gitlab.aliakseikaraliou.oner.vk.models.draft

data class VkDraftConversation(val peerId: Long,
                               val lastMessage: VkDraftMessage?,
                               val unreadCount: Int)