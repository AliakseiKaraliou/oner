package com.gitlab.aliakseikaraliou.oner.vk.network.deserializers

import com.gitlab.aliakseikaraliou.oner.vk.VkConstants
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import java.lang.reflect.Type

abstract class BaseVkDeserializer<T> : JsonDeserializer<T> {
    
    abstract fun deserialize(json: JsonElement): T
    
    final override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): T {
        json?.asJsonObject?.get(VkConstants.Json.RESPONSE)?.let {
            return deserialize(it)
        } ?: run {
            throw VkErrorDeserializer().deserialize(json)
        }
    }
}