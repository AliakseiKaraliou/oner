package com.gitlab.aliakseikaraliou.oner.vk.utils.exceptions

open class VkException(val code: Int?,
                       override val message: String?) : Exception(message)