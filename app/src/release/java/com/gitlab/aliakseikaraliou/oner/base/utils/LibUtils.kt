package com.gitlab.aliakseikaraliou.oner.base.utils

import android.app.Application
import com.squareup.leakcanary.LeakCanary

object LibUtils {
    @Suppress("UNUSED_PARAMETER")
    fun init(application: Application) {
        LeakCanary.install(application)
    }
}