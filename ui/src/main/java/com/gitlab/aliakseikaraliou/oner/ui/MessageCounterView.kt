package com.gitlab.aliakseikaraliou.oner.ui

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.view.View

class MessageCounterView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null) : View(context, attrs) {
    private val textPaint = Paint().apply {
        textSize = 20 * resources.displayMetrics.scaledDensity
    }

    private val roundPaint = Paint().apply {
        color = Color.BLACK
    }

    var value: Int = 0
        set(value) {
            if (field != value) {
                field = value
                invalidate()
            }
        }

    private val stringValue: String
        get() {
            if (value < K_VALUE) {
                return value.toString()
            }

            var tempValue = value.toDouble()
            val kBuilder = StringBuilder()

            while (tempValue >= K_VALUE) {
                kBuilder.append("K")
                tempValue /= K_VALUE
            }

            return "%.1f $kBuilder".format(tempValue)
        }

    init {
        context.obtainStyledAttributes(attrs, R.styleable.MessageCounterView, 0, 0)?.let { typedArray ->
            try {
                value = typedArray.getInt(R.styleable.MessageCounterView_value, 0)

                textPaint.apply {
                    color = typedArray.getInt(R.styleable.MessageCounterView_textColor, Color.WHITE)
                }

                roundPaint.apply {
                    color = typedArray.getInt(R.styleable.MessageCounterView_backgroundColor, Color.BLACK)
                }
            } finally {
                typedArray.recycle()
            }
        }
    }

    @SuppressLint("DrawAllocation")
    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)

        val heightSpec = MeasureSpecification(heightMeasureSpec)
        val widthSpec = MeasureSpecification(widthMeasureSpec)

        var textHeight = when (heightSpec.mode) {
            MeasureSpecification.MeasureMode.EXACTLY, MeasureSpecification.MeasureMode.AT_MOST -> {
                textPaint.textSize = heightSpec.size.toFloat() / HEIGHT_COEF

                heightSpec.size / HEIGHT_COEF

            }
            MeasureSpecification.MeasureMode.UNSPECIFIED -> {
                measureText(stringValue, textPaint).height
            }
        }

        when (heightSpec.mode) {
            MeasureSpecification.MeasureMode.EXACTLY, MeasureSpecification.MeasureMode.AT_MOST -> {
                var measureText = measureText(stringValue, textPaint)

                while (measureText.width * WIDTH_COEF > widthSpec.size) {
                    textPaint.textSize--

                    measureText = measureText(stringValue, textPaint)

                    textHeight = measureText.height
                }

                measureText.width
            }
            MeasureSpecification.MeasureMode.UNSPECIFIED -> {
                measureText(stringValue, textPaint).width
            }
        }

        val ovalSizeDifference = measureText(stringValue.substring(1), textPaint)

        setMeasuredDimension(textHeight * HEIGHT_COEF + ovalSizeDifference.width, textHeight * HEIGHT_COEF)
    }

    @SuppressLint("DrawAllocation")
    override fun onDraw(canvas: Canvas) {
        val size = Size(height = height, width = width)

        val centerWidth = size.width.toFloat() / 2
        val centerHeight = size.height.toFloat() / 2

        val rectF = RectF(0f, 0f, size.width.toFloat(), size.height.toFloat())
        canvas.drawOval(rectF, roundPaint)

        val textSize = measureText(stringValue, textPaint)
        canvas.drawText(stringValue,
                centerWidth - textSize.width / 2,
                centerHeight + textSize.height / 2,
                textPaint)
    }

    companion object {
        private const val K_VALUE = 1000

        private const val HEIGHT_COEF = 2
        private const val WIDTH_COEF = 1.1
    }

}