package com.gitlab.aliakseikaraliou.oner.ui

import android.graphics.Paint
import android.graphics.Rect
import android.view.View

internal fun recalculateSize(measured: Int, spec: Int): Int {
    val specification = MeasureSpecification(spec)

    return when (specification.mode) {
        MeasureSpecification.MeasureMode.EXACTLY -> specification.size
        MeasureSpecification.MeasureMode.AT_MOST -> Math.min(measured, specification.size)
        MeasureSpecification.MeasureMode.UNSPECIFIED -> measured
    }
}

internal data class Size(val width: Int, val height: Int) {
    fun max() = Math.max(width, height)

    operator fun times(times: Int) = Size(width * times, height * times)
}

internal fun measureText(text: String, paint: Paint): Size {
    val rect = Rect()
    paint.getTextBounds(text, 0, text.length, rect)

    return Size(width = paint.measureText(text).toInt(), height = rect.height())
}

internal data class MeasureSpecification(val size: Int, val mode: MeasureMode) {

    constructor(spec: Int) : this(size = View.MeasureSpec.getSize(spec),
            mode = MeasureMode.modeFromIntValue(intValue = View.MeasureSpec.getMode(spec)))

    internal enum class MeasureMode(val value: Int) {
        EXACTLY(View.MeasureSpec.EXACTLY),
        AT_MOST(View.MeasureSpec.AT_MOST),
        UNSPECIFIED(View.MeasureSpec.UNSPECIFIED);

        companion object {
            fun modeFromIntValue(intValue: Int) = MeasureMode.values().first { it.value == intValue }
        }
    }
}